#include "stdafx.h"
#include "AppearanceOfRecipient.h"
#include "Recipients.h"
#include "DEBUG.h"
#include "Calendar.h"
#include "Distribution.h"
#include <iostream>
#include <string>

AppearanceOfRecipient::AppearanceOfRecipient(string const name, BloodBank* blood_bank, int const t0) :TimeCondition(name, blood_bank, t0){};


void AppearanceOfRecipient::Execute(BloodBank* blood_bank)
{
	
	int need = static_cast<int>(blood_bank->get_generate()->rand_need_blood_for_recipient(1 / t_g_)); //zmienna kt�ra b�dzie brana z generatora
	//if (need >= 30)need = 13;	//warunek aby symulacja si� nie zatrzyma�a
	int time = static_cast<int>(round(blood_bank->get_generate()->rand_time_between_appearance_recipient(p_g_)));
	if (time == 0)time++;
	if (DEBUG)std::cout << "Add receipien to queue who need " << std::to_string(need) << " unit of blood. Nest is planing at " << time<<" ."<< std::endl;
	blood_bank->inc_handled_recipients();
	blood_bank->GettingToQueue(new Recipients(need));
	if(blood_bank->length_of_longest_recipient_queue < blood_bank->queue_recipients_size())	//zapisaywanie maksymalnej d�ugo�ci kolejki
	{
		blood_bank->length_of_longest_recipient_queue = blood_bank->queue_recipients_size();
	}
	//iteracja ilo�ci pacjent�w kt�rzy weszli do banku krwi
	blood_bank->calendar_->AddSortTimeCondition(new AppearanceOfRecipient("Planing add to queue.", blood_bank, time+ blood_bank->calendar_->get_time_system()));
}