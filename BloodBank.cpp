#include "stdafx.h"
#include "BloodBank.h"
#include <iostream>
#include  "UnitOfBlood.h"
#include <queue>
#include <list>
#include "Calendar.h"
#include "Utilization.h"
#include "DEBUG.h"


BloodBank::BloodBank(int const r, int const j) : R_(r), J(j),  pool_ID(0), EO(false), NO(false), number_of_order_emengency_(0),number_of_order_all_(0),number_of_blood_utylization_(0) {
  if(DEBUG)std::cout << "Made a new blood bank" << std::endl;
  calendar_ = new Calendar(this);
  generate_ = new Distribution();
  number_of_blood_spent_ = 0;
  number_of_recipients_served=0;
  length_of_longest_recipient_queue=0;
}
BloodBank::~BloodBank()
{
	delete calendar_;
	delete generate_;
	
}
void BloodBank::inc_number_of_order_all()
{
	number_of_order_all_++;
}
void BloodBank::inc_number_of_order_emengency()
{
	number_of_order_emengency_++;
}
void BloodBank::inc_number_of_blood_spent()
{
	number_of_blood_spent_++;
}
int BloodBank::get_number_of_order_emengency() const
{
	return number_of_order_emengency_;
}
int BloodBank::get_number_of_blood_spent() const
{
	return number_of_blood_spent_;
}
int BloodBank::get_number_of_order_all() const
{
	return number_of_order_all_;
}
void BloodBank::inc_number_of_blood_utylization()
{
	number_of_blood_utylization_++;
}
int BloodBank::get_number_of_blood_utylization() const
{
	return number_of_blood_utylization_;
}
void BloodBank::reseting_statistic()
{
	number_of_blood_spent_ = 0;
	number_of_blood_utylization_ = 0;
	number_of_order_all_ = 0;
	number_of_order_emengency_ = 0;
}

int BloodBank::get_R() const
{
	return R_;
}
Distribution* BloodBank::get_generate() const 
{
	return generate_;
}

void BloodBank::AddingBloodUnits(const int t0) {
  AddBloodUnitsSort(UnitOfBlood(pool_ID, t0));
  int time_Execute = calendar_->get_time_system() + t0;
  J++;
  calendar_->AddSortTimeCondition(new Utilization("Utilization", this, time_Execute, pool_ID));	//automatyczne zaplanowanie utylizacji
  if (DEBUG)std::cout << "Planing Utilization at " << time_Execute << std::endl;
  pool_ID++;
	
}
void BloodBank::ReadList()  {
  if(DEBUG)std::cout << "Read list units of blood:" << std::endl;
  for (list< UnitOfBlood>::iterator  i = queue_blood.begin(); i != queue_blood.end(); ++i) 
  {
    if(DEBUG)std::cout << int((*i).get_id()) << "  " << int((*i).get_t0()) << std::endl;
  }
}
void  BloodBank::AddBloodUnitsSort(UnitOfBlood unit_of_blood) {
  if (queue_blood.empty() == true)  queue_blood.push_front(unit_of_blood);
  else
  {
    for (list<UnitOfBlood>::iterator i = queue_blood.begin(); i != queue_blood.end(); ++i)
    {
      if (int((*i).get_t0()) > unit_of_blood.get_t0())
      {
        queue_blood.insert(i, unit_of_blood);
        return;
      }
    }
    queue_blood.push_back(unit_of_blood);
  }
}
bool BloodBank::DeleteIdUnitsOfBlood(const int id) { //usuwanie jednostki krwi z listy po ID
  for (list<UnitOfBlood>::iterator i = queue_blood.begin(); i != queue_blood.end(); ++i)
  {
    if ((*i).get_id() == id)
    {
      queue_blood.erase(i);
	  J--;
      if(DEBUG)std::cout << "Find and delete search unit of blood" << endl;
	  calendar_->DeleteTimeConditionUtilization(id);
      return true;
    }
  }
  if (DEBUG) cout << "No find search unit of blood" << endl;
  return false;
}



void BloodBank::GettingToQueue(Recipients* recipients)
{
	queue_of_recipients_.push(recipients);
}
void BloodBank::ReadQueue() const
{
	if (DEBUG) std::cout << "First patient on queue need:" << queue_of_recipients_.front()->A << " units of blood" << std::endl;
}
void BloodBank::DeleteFirstQueue()
{
	queue_of_recipients_.pop();
	if (DEBUG) std::cout << "Delete first patient on queue" << std::endl;
}


Recipients* BloodBank::get_first_recipient() const
{
	return queue_of_recipients_.front();
}

bool BloodBank::deleteFirstUnitsOfBlood()
{
	J--;
	if (queue_blood.empty()) return false;
	if(calendar_->DeleteTimeConditionUtilization(queue_blood.front().get_id()))
	{
		queue_blood.pop_front();
		return true;
	}
	if (DEBUG)std::cout << "ERROR. Not find Utilization on calender." << std::endl;
	return false;
}
int BloodBank::get_id_FirstUnitOfBlood() 
{
	return (queue_blood.front()).get_id();
}

int BloodBank::get_size_blood_in_bank() const
{
	return  static_cast<int>(queue_blood.size());
}

int BloodBank::get_handled_recipients() const
{
	return handled_recipients_;
}

void BloodBank::inc_handled_recipients()
{
	handled_recipients_++;
}
int BloodBank::queue_recipients_size() const
{
	return static_cast<int>(queue_of_recipients_.size());
}