#pragma once
#include "stdafx.h"
#include  "UnitOfBlood.h"
#include <list>
#include <queue>
#include "Recipients.h"
#include "Distribution.h"

class Calendar;

class BloodBank { //klasa Bank krwi
public:
  int J;  //liczba  aktualnych jednostek krwi
  bool EO;  //flaga blokowania zam�wie� awartujnych
  bool NO;  //flaga blokowania zam�wie� normalnych
  int get_R() const;


//Dane do statystyk oraz warunk�w ko�ca

  int number_of_recipients_served;
  int length_of_longest_recipient_queue;


//funkcje odpowiedzialne za kolejk� z krwi�
  void ReadList();
  bool DeleteIdUnitsOfBlood(int id);  //usuwanie jednistki krwi o danym ID z banku krwi
  void AddingBloodUnits(int t0);
  bool deleteFirstUnitsOfBlood();
  int get_id_FirstUnitOfBlood() ;
  int get_size_blood_in_bank() const;
  int get_handled_recipients()const;
  int queue_recipients_size()const;
  void inc_handled_recipients();
  Calendar* calendar_;
  

//funkcje odpowiedzialne za obs�ug� kolejki z biorcami
  void GettingToQueue(Recipients* recipients);
  void ReadQueue() const;
  void DeleteFirstQueue();
//funkcje odpowiedzialne za zbieranie statystyk
  void inc_number_of_order_emengency();	//liczba zam�wie� awaryjnych
  void inc_number_of_order_all();	//liczba zam�wie� ca�kowita
  void inc_number_of_blood_utylization();	//liczba krwi zutylizowana
  void inc_number_of_blood_spent();	//liczba krwi wydana pacjentom
  int get_number_of_order_emengency() const;	//liczba zam�wie� awaryjnych
  int get_number_of_order_all() const;	//liczba zam�wie� ca�kowita
  int get_number_of_blood_utylization() const;	//liczba krwi zutylizowana
  int get_number_of_blood_spent() const;	//liczba krwi wydana pacjentom
  void reseting_statistic();

  Recipients* get_first_recipient() const;
//funkcje odpowiedzialne za generacje liczb 
  Distribution* get_generate() const;
//
  BloodBank(int r, int j);
  ~BloodBank();
private:
  Distribution * generate_;
  int number_of_blood_spent_;
  int number_of_order_emengency_;
  int number_of_order_all_;
  int number_of_blood_utylization_;

  int pool_ID;  //pocz�tek adres�w ID
  int R_;  //poziom krwi od kt�rego wysy�amy zam�wienie na dostaw�
  int handled_recipients_;
  std::list<UnitOfBlood> queue_blood;  //lista wska�nik�w na jednostki krwi
  std::queue<Recipients*> queue_of_recipients_;
  void AddBloodUnitsSort(UnitOfBlood unit_of_blood);
};
