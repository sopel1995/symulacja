#include "stdafx.h"
#include "Distribution.h"
#include <cmath>
#include <string>
#include <fstream>
#include<fstream>
#include<sstream>
#include <iostream>


Distribution::Distribution()
{
	
	read_kernel(); //zczytanie ziaren z pliku
	activate_kernels(0);
	a_ = 16807;
	q_ = 127773;
	m_ = 2147483647;
	r_ = 2836;
	kernel_time_order_normal_ = 0;
	kernel_time_between_appearance_recipient_ = 0;
	kernel_need_blood_for_recipient_ = 0;
	kernel_time_order_emergency_ = 0;
	kernel_time_appearance_donation_ = 0;
	kernel_promotion_t_frequency_ = 0;
	kernel_promotion_t_difference_ = 0;
}
Distribution::Distribution(int kernels)
{
	a_ = 16807;
	q_ = 127773;
	m_ = 2147483647;
	r_ = 2836;
	kernel_time_order_normal_ = 0;
	kernel_time_between_appearance_recipient_ = 0;
	kernel_need_blood_for_recipient_ = 0;
	kernel_time_order_emergency_ = 0;
	kernel_time_appearance_donation_ = 0;
	kernel_promotion_t_frequency_ = 0;
	kernel_promotion_t_difference_ = 0;
}

double Distribution::EvenGenerator(int &kernel)
{
	
	int const  h = kernel /q_;
	kernel = a_ * (kernel - q_ * h) - r_ * h;
	if (kernel < 0) kernel = kernel + (m_);
	return (static_cast<double>(kernel)/ m_);
}
double Distribution::EvenGenerator(int &kernel, const int start, const int end)
{
	return (EvenGenerator(kernel)*(end - start) + start);
}

double Distribution::ExponentialGenerator(int &kernel, const int ave)
{
	double const e = EvenGenerator(kernel);		//metoda odwróconej dystrybuanty
	return (-ave*log(e));

}
double Distribution::NormalGenerator(int &kernel, const double m, const double d)
{	
	double u2;
	double x;
	do
	{
		const double u1 = EvenGenerator(kernel);
		u2 = EvenGenerator(kernel);
		x = -log(u1);
	} while (!(u2 <= exp(-(pow(x - 1, 2) / 2))));
	if (EvenGenerator(kernel) < 0.5) x = x * -1.0;
	return (x*d +m);
}

double Distribution::GeometricGenerator(int &kernel, double const p)
{
	int i = 0;
	while((1/p) < EvenGenerator(kernel))
	{
		i++;
	}
	return i + 1;
}

double Distribution::rand_time_order_normal(const int ave)	
{
	return ExponentialGenerator(kernel_time_order_normal_, ave);
}

double Distribution::rand_time_between_appearance_recipient(const int ave)
{
	return ExponentialGenerator(kernel_time_between_appearance_recipient_, ave);
}

double Distribution::rand_need_blood_for_recipient(double const p)
{
	return GeometricGenerator(kernel_need_blood_for_recipient_, p);
}

double Distribution::rand_time_order_emergency(double const m, double const d)
{
	return NormalGenerator(kernel_time_order_emergency_, m, d);
}

double Distribution::rand_time_appearance_donation(double const p)
{
	return ExponentialGenerator(kernel_time_appearance_donation_, static_cast<int>(p));
}

double Distribution::rand_promotion_t_frequency(int const start, int const end)
{
	return EvenGenerator(kernel_promotion_t_frequency_, start, end);
}

double Distribution::rand_promotion_t_difference(int const start, int const end)
{
	return EvenGenerator(kernel_promotion_t_difference_, start, end);
}

void Distribution::read_kernel()
{
	std::fstream plik;

	std::string text;
	plik.open("kernels.txt", std::ios::in | std::ios::out);
	std::istringstream is(text);
	const int num_simulate = 10;

	if (plik.good())
	{
		
		for (int i = 0; i < num_simulate; i++)
		{
			if (plik.eof())break;
			for (int j = 0; j < 7; j++)
			{
			
				std::getline(plik, text, ' ');
				kernels_tab_[i][j] = std::stoi(text);
				is >> text;
			}
			
		}

		plik.close();
	}else std::cout << "Error! Nie udalo otworzyc sie pliku!" << std::endl;
}

void Distribution::activate_kernels(int const i)
{	
	if(i>10)
	{
		std::cout << "ERROR KERNELS" << std::endl;
		return;
	}
	kernel_time_order_normal_ = kernels_tab_[i][0];
	kernel_time_between_appearance_recipient_ = kernels_tab_[i][1];
	kernel_need_blood_for_recipient_ = kernels_tab_[i][2];
	kernel_time_order_emergency_ = kernels_tab_[i][3];
	kernel_time_appearance_donation_ = kernels_tab_[i][4];
	kernel_promotion_t_frequency_ = kernels_tab_[i][5];
	kernel_promotion_t_difference_ = kernels_tab_[i][6];
}
void Distribution::set_difference_donation(int const difference_donation)
{
	 difference_donation_= difference_donation;
}
int Distribution::get_difference_donation() const
{
	return difference_donation_;
}