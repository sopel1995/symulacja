#pragma once
class Distribution
{
public:
	double EvenGenerator(int &kernel);
	double EvenGenerator(int &kernel, int start,  int end);
	double ExponentialGenerator(int &kernel, int ave);
	double NormalGenerator(int &kernel, double m, double d);
	double GeometricGenerator(int &kernel, double p);
	double rand_time_order_normal(int ave);
	double rand_time_between_appearance_recipient(int ave);
	double rand_need_blood_for_recipient(double p);
	double rand_time_order_emergency(double m, double d);
	double rand_time_appearance_donation(double p);
	double rand_promotion_t_frequency(int start, int end);
	double rand_promotion_t_difference(int start, int end);
	void activate_kernels(int i);
	void read_kernel();
	void set_difference_donation(int difference_donation);
	int get_difference_donation() const;
	Distribution();
	Distribution(int kernels);
	~Distribution()=default;
private:
	int kernels_tab_[10][7];
	int kernel_time_order_normal_;
	int kernel_time_between_appearance_recipient_;
	int kernel_need_blood_for_recipient_;
	int kernel_time_order_emergency_;
	int kernel_time_appearance_donation_;
	int kernel_promotion_t_frequency_;	//ziarno dla losowań częstotliwości występowania promocji 
	int kernel_promotion_t_difference_;		//ziarno dla losowań spadku czasu między dawcami
	int a_;
	int q_;
	int m_;
	int r_;
	int difference_donation_;
};

