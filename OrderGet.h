#pragma once
#include "TimeCondition.h"
#include "BloodBank.h"


class OrderGet :
	public TimeCondition
{
public:
	void Execute(BloodBank* blood_bank) override;
	OrderGet(string name, BloodBank* blood_bank, int t0, int q,bool normal);
	~OrderGet()=default;
private:
	int Q_;  //liczba jednostek krwi zamówiona w zamówieniu
	int T0_=1000; //czas ważności krwi przy zamówieniu 
	bool normal_;

};