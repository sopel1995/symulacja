#include "stdafx.h"
#include "Promotion.h"
#include "Calendar.h"

Promotion::Promotion(BloodBank* blood_bank, int const t0 ,bool const start) : TimeCondition("Donor's blood", blood_bank, t0)
{
	start_ = start;

}



void Promotion::Execute(BloodBank* blood_bank)
{

	if(start_)
	{
		blood_bank->get_generate()->set_difference_donation(static_cast<int>(blood_bank->get_generate()->rand_promotion_t_difference(s_d_,e_d_)));
		int const t = tt_ + blood_bank->calendar_->get_time_system();
		blood_bank->calendar_->AddSortTimeCondition(new Promotion(blood_bank, t, false));	//zaplanowanie zako�czenia promocji
	}else
	{
		blood_bank->get_generate()->set_difference_donation(0);
		int const t = static_cast<int>(blood_bank->get_generate()->rand_promotion_t_frequency(s_f_, e_f_) + blood_bank->calendar_->get_time_system());
		blood_bank->calendar_->AddSortTimeCondition(new Promotion(blood_bank, t, true));	//zaplanowanie nast�nej promocji
	}
}