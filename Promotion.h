#pragma once
#include "TimeCondition.h"


class Promotion :
	public TimeCondition
{
public:
	void Execute(BloodBank* blood_bank) override;
	Promotion(BloodBank* blood_bank, int t0, bool start);
	~Promotion() = default;
private:
	bool start_;
	int tt_ = 7200;
	int s_f_ = 20000;
	int e_f_ = 22000;
	int s_d_ = 100;
	int e_d_ = 200;
};

