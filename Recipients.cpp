#include "stdafx.h"
#include "Recipients.h"
#include "DEBUG.h"
#include <iostream>


Recipients::Recipients(int const a) :A(a) {
  if (DEBUG) std::cout << "Made recipients who need: " << A << "units of blood" << std::endl;
}
