#include "stdafx.h"
#include "SendingBloodOrder.h"
#include "Calendar.h"
#include "OrderGet.h"
#include <string>
#include "DEBUG.h"
#include <iostream>


SendingBloodOrder::SendingBloodOrder(string const name, BloodBank* blood_bank, int const Q, bool const normal) :TimeCondition(name, blood_bank, 0), normal(normal) {
	int delivery_time;
	
	if (normal)
	{
		TN_ = static_cast<int>(round(blood_bank->get_generate()->rand_time_order_normal(n_g_)));
		if (TN_ == 0)TN_++;
		delivery_time = TN_ + blood_bank->calendar_->get_time_system();
		if (DEBUG)std::cout << "NORMAL" << endl;
		blood_bank->calendar_->AddSortTimeCondition(new OrderGet("Order", blood_bank, delivery_time,Q, true));
	}
	else
	{
		TE_ = static_cast<int>(round(blood_bank->get_generate()->rand_time_order_emergency(e_m_g_, e_d_g_)));
		if (TE_ == 0)TE_++;
		delivery_time = TE_ + blood_bank->calendar_->get_time_system();
		if (DEBUG)std::cout << "EMERGENCY" << endl;
		blood_bank->calendar_->AddSortTimeCondition(new OrderGet("Order", blood_bank, delivery_time, Q_E_,false));
	}
	if (DEBUG)std::cout << "Sending Blood Order to " + std::to_string(Q_E_) + " units of blood. Delivery time is: " << delivery_time << "." << std::endl;
}

int SendingBloodOrder::get_TE() const
{
	return TE_;
}
int SendingBloodOrder::get_TN() const
{
	return TN_;
}


void SendingBloodOrder::Execute(BloodBank* blood_bank) {
  if (normal) {
	  TN_=static_cast<int>(round(blood_bank->get_generate()->rand_time_order_normal(n_g_)));
    blood_bank->calendar_->AddSortTimeCondition(new OrderGet("OrderGet", blood_bank, TN_+blood_bank->calendar_->get_time_system(), Q_N_,true));
  }
  else {
	  TE_ = static_cast<int>(round(blood_bank->get_generate()->rand_time_order_emergency(e_m_g_, e_d_g_)));
	  blood_bank->calendar_->AddSortTimeCondition(new OrderGet("OrderGet", blood_bank, TE_ + blood_bank->calendar_->get_time_system(), Q_E_,false));
  }
}
