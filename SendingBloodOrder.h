#pragma once
#include "TimeCondition.h"
#include "OrderGet.h"

class SendingBloodOrder ://klasa odpowiedzialna za wysy�anie zam�wienia awaryjne
  public TimeCondition
{

public:
  bool normal;
  int get_TE() const;
  int get_TN() const;

  void Execute(BloodBank* blood_bank) override;
  SendingBloodOrder(string name, BloodBank* blood_bank, int Q, bool normal);
  ~SendingBloodOrder()=default;
private:
	int TN_;	//czas na przybycie zam�wienia
	int TE_;
	int Q_N_ = 60;	//nieaktywna bo przez konstruktor SendinBlood
	int Q_E_ = 16;
	int n_g_ = 1800;	
	int e_m_g_ = 300;	//warto�� �rednia
	double e_d_g_ = 0.1;	//wariancja
	
};

