#include "stdafx.h"
#include "TimeCondition.h"
#include "BloodBank.h"
#include "DEBUG.h"
#include <iostream>


TimeCondition::TimeCondition(string const name, BloodBank* blood_bank, int const time_Execute) : name_(name), blood_bank_(blood_bank), time_execute_(time_Execute)
{
}


void TimeCondition::Execute(BloodBank* blood_bank) {
  if (DEBUG)std::cout << "TimeCondition::Execute(BloodBank* blood_bank)" << endl;
}

string TimeCondition::ReadName() const
{
	return  name_;
}
int TimeCondition::get_execution_time() const
{
	return time_execute_;
}
