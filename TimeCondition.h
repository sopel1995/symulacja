#pragma once
#include "stdafx.h"
#include "BloodBank.h"
using namespace std;

class TimeCondition //klasa bazowa wszystkich zdarze� czasowych
{
public:
  int get_execution_time() const;	//wydobycie czasu wykonania zdarzenia 
  string ReadName() const;	//funkcja s�u��ca do odczytania nazwy zdarzenia
  virtual void Execute(BloodBank* blood_bank);
  TimeCondition(string name, BloodBank* blood_bank, int time_Execute);
  virtual ~TimeCondition()=default;

private:
	string name_;
	BloodBank* blood_bank_;
	int time_execute_;

};
