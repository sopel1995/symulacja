#include "stdafx.h"
#include "UnitOfBlood.h"


UnitOfBlood::UnitOfBlood(int const id, int const t0) : id_(id), t0_(t0)
{
}

int UnitOfBlood::get_id() const
{
	return id_;
}
int UnitOfBlood::get_t0() const
{
	return t0_;
}
void UnitOfBlood::set_id(int const id)
{
	id_ = id;
}
void UnitOfBlood::set_t0(int const t0)
{
	t0_ = t0;
}

