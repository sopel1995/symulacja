﻿#pragma once

class UnitOfBlood {
public:
	int get_id() const;
	int get_t0() const;
	void set_id(int id);
	void set_t0(int t0);

	UnitOfBlood(int id, int t0);
	~UnitOfBlood() = default;
private:
	int id_;
	int t0_; //czas na zużycie jednostki krwi
};


