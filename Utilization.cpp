#include "stdafx.h"
#include "Utilization.h"
#include "DEBUG.h"
#include <iostream>


Utilization::Utilization(string const name, BloodBank* blood_bank, int const execution_time, int const id) :TimeCondition(name, blood_bank, execution_time), id_(id)
{
}

void Utilization::Execute(BloodBank* blood_bank) {

	blood_bank->DeleteIdUnitsOfBlood(id_);
	blood_bank->inc_number_of_blood_utylization();
	if(DEBUG)std::cout << "Utilization." << endl;
}

int Utilization::get_id() const
{
	return id_;
}

