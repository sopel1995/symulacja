#pragma once
#include "stdafx.h"
#include "BloodBank.h"
#include "TimeCondition.h"


class Utilization : //klasa odpowiedzialna za utylizację krwi
  public TimeCondition
{
public: 
  void Execute(BloodBank* blood_bank) override;
  int get_id() const;
  Utilization(string name, BloodBank* blood_bank, int execution_time, int id);
  ~Utilization()=default;
private:
	int id_;
};

