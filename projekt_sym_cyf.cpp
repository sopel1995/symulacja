// projekt_sym_cyf.cpp : Defines the entry point for the console application.
#include "stdafx.h"
#include <iostream>
#include <list>
#include <fstream>
#include "BloodBank.h"
#include "Calendar.h"
#include "DonationByDose.h"
#include "SendingBloodOrder.h"
#include "AppearanceOfRecipient.h"
#include <conio.h>
#include "Distribution.h"
#include "DEBUG.h"
#include "Promotion.h"
#include <string>

void simulate();
void test();
void write_state(fstream &state, int time_system, int number_of_recipients_served, int number_of_blood_spent, int act_length_queue_of_recipients, bool act_emergency_order, int j, int act_calendar_size) ;
void generate_kernels( int num, int first_kernel);
char buff[100000];
int main() {
	std::cout << "Please choose the taks: 1-simulate, 2-test" << endl;
	int choose;
	cin >> choose;
	switch(choose)
	{
	case 1:
		simulate();
		break;
	case 2:
		test();
		break;
	default:
		std::cout << "Bad value" << endl;
		break;
	}

	return 0;
}
void simulate()
{

	//Ustawienie warunków końca symulacji
	std::cout << "Please check end condition: 1-number of recipients served, 2-number of blood spent, 3-number of order spent." << endl;
	int end_condition;
	cin >> end_condition;
	int end_number;
	switch(end_condition)
	{
	case 1:
		std::cout << "Please write number of recipients served " << endl;
		cin >> end_number;
		break;
	case 2:
		std::cout << "Please write number of blood spent " << endl;
		cin >> end_number;
		break;
	case 3:
		std::cout << "Please write number of order spent " << endl;
		cin >> end_number;
		break;
	default:
		std::cout << "Bad value" << endl;
		break;
	}
	std::cout << "Please write number of simulation." << endl;
	int number_of_simulation;
	cin >> number_of_simulation;
	generate_kernels(number_of_simulation, 55000);
	fstream state("state_simulation.txt", ios::out);

	for (int r =5; r < 31; r+=5)	//pętla odpowiedzialna za zmianę parametru R
	{
		fstream result_file("result_R=" + std::to_string(r) + ".txt", ios::out);
		for (int q_n = 10; q_n <31; q_n += 5)		//pętla odpowiedzialna za zmianę parametru q_n
		{
			

			std::cout << '\n' << '\n' << "R= " << r << " Q_N= "<<q_n<<'\n';
			result_file << '\n' << '\n' << "R=" << r << " Q_N= " << q_n << '\n';

			for (int iter_simulate_number = 0; iter_simulate_number < number_of_simulation; iter_simulate_number++)
			{
				 if(ACCURATE_STATISTICS)fstream state("state_simulation"+ std::to_string(iter_simulate_number)+".txt", ios::out);
				BloodBank *blood_bank = new BloodBank(r, 0);
				blood_bank->calendar_->AddSortTimeCondition(new AppearanceOfRecipient("Pierwszy", blood_bank, 0));
				blood_bank->calendar_->AddSortTimeCondition(new DonationByDose(blood_bank, 0));
				blood_bank->calendar_->AddSortTimeCondition(new Promotion(blood_bank, 0, true));
				blood_bank->get_generate()->activate_kernels(iter_simulate_number);
				if (DEBUG)std::cout << "System time: " << blood_bank->calendar_->get_time_system() << " State: blood_units= " << blood_bank->J << ", recipien_in_queue= " << blood_bank->queue_recipients_size() << ", handled_recipients=" << blood_bank->get_handled_recipients() << "." << std::endl;
				std::cout << "Start symulation ->" << endl;
				char z = '0';
				int old_time = 0;
				int old_event = 0;

				bool end_simulate = false;
				bool reset_done = false;
				int const end_initial_phase = 120000;
				bool repeat = true;
				for (list<TimeCondition*>::iterator i = blood_bank->calendar_->calendar.begin(); !end_simulate; ++i) {
					blood_bank->calendar_->set_time_System((*i)->get_execution_time());
					if (old_time != blood_bank->calendar_->get_time_system() && blood_bank->calendar_->get_time_system() > end_initial_phase&& ACCURATE_STATISTICS)
					{
						write_state(state, blood_bank->calendar_->get_time_system(), blood_bank->number_of_recipients_served, blood_bank->get_number_of_blood_spent(), blood_bank->queue_recipients_size(), blood_bank->EO, blood_bank->J, blood_bank->calendar_->CalendarSize());	//tylko gdy zmienił się czas to zapisuj
					}
						if (!reset_done && blood_bank->calendar_->get_time_system() > end_initial_phase)	//reset po przejściu fazy początkowej
					{
						reset_done = true;
						blood_bank->reseting_statistic();
					}
					if (old_time < blood_bank->calendar_->get_time_system())	//system usuwania przestarzałych zdarzeń 
					{
						for (int i = 0; i < old_event; i++)
						{
							blood_bank->calendar_->DeleteTimeConditionFirst();
						}
						old_event = 0;
					}
					else old_event++;
					old_time = blood_bank->calendar_->get_time_system();

					if (STEP)
					{
						while (!(z == 13))	//czekanie na wciśniecie ENTER
						{
							z = _getch();
						}z = 'O';

					}
					if (DEBUG)std::cout << "////////////////////////////////////////" << std::endl;
					if (DEBUG)std::cout << "System time: " << blood_bank->calendar_->get_time_system() << " State: blood_units= " << blood_bank->J << ", recipien_in_queue= " << blood_bank->queue_recipients_size() << ", handled_recipients=" << blood_bank->get_handled_recipients() << "." << std::endl;
					if (DEBUG)std::cout << "->";

					(*i)->Execute(blood_bank);
					//IfConditions
					do
					{
						repeat = false;

						if (blood_bank->J < blood_bank->get_R() && !(blood_bank->NO))
						{
							blood_bank->calendar_->AddSortTimeCondition(new SendingBloodOrder("Normal Order", blood_bank, q_n, true));
							blood_bank->NO = true;
							if (DEBUG)std::cout << "If Condition: Normal" << std::endl;
							blood_bank->inc_number_of_order_all();
							repeat = true;
						}

						if (blood_bank->queue_recipients_size() > 0)	//obsługa pierwszego w kolejce
						{
							if (blood_bank->J >= blood_bank->get_first_recipient()->A)
							{
								for (int i = 0; i < blood_bank->get_first_recipient()->A; i++)
								{
									if (blood_bank->deleteFirstUnitsOfBlood())blood_bank->inc_number_of_blood_spent();
									else
									{
										if (DEBUG)std::cout << "ERROR. Not delete first unit of blood." << std::endl;
									}
								}
								if (DEBUG)std::cout << "First recipient servised" << std::endl;
								blood_bank->DeleteFirstQueue();
								blood_bank->number_of_recipients_served++;
								repeat = true;
							}
							else  //ponów próbę transfuzji krwi w natępnej iteracji kalendarza
							{
								if (DEBUG)std::cout << "WAIT FOR UnitOfBlood !!!" << std::endl;
								if (!(blood_bank->EO))
								{
									blood_bank->inc_number_of_order_all();
									blood_bank->inc_number_of_order_emengency();
									blood_bank->calendar_->AddSortTimeCondition(new SendingBloodOrder("Emergency Order", blood_bank, blood_bank->calendar_->get_time_system(), false));
									blood_bank->EO = true;
									if (DEBUG)std::cout << "If Condition: Emergency" << std::endl;
									repeat = false;
								}
							}
						}

						if (DEBUG)std::cout << std::endl;
					} while (repeat);

					//Warunki końca przebiegu symulacyjnego
					if ((end_condition) == 1 && blood_bank->number_of_recipients_served >= end_number) end_simulate = true;
					if ((end_condition) == 2 && blood_bank->get_number_of_blood_spent() >= end_number) end_simulate = true;
					if ((end_condition) == 3 && blood_bank->get_number_of_order_all() >= end_number) end_simulate = true;

				}
				std::cout << "End symulation" << endl;

				std::cout << "State: number_of_recipients_served=" << blood_bank->number_of_recipients_served << "	number_of_blood_spent=" << blood_bank->get_number_of_blood_spent() << "	length_of_longest_recipient_queue=" << blood_bank->length_of_longest_recipient_queue << std::endl;
				
				double const p_e_o = (static_cast<double>(blood_bank->get_number_of_order_emengency()) / static_cast<double>(blood_bank->get_number_of_order_all()))*100.0;
				double const p_u = ((static_cast<double>(blood_bank->get_number_of_blood_utylization()) / static_cast<double>(blood_bank->get_number_of_blood_utylization() + blood_bank->get_number_of_blood_spent())))*100.0;
				std::cout << "Probability of send emergency order is equal " << p_e_o << endl;
				std::cout << "Probability of Utilization blood is equal " << p_u << " %." << endl;
				result_file << ' '<< p_e_o << ' ' << p_u<<'\n';
				delete blood_bank;
				if(ACCURATE_STATISTICS)state.close();
			}
			
		}
		result_file.close();
	}
	
	std::cout << "Please click escape " << endl;

	char z = 50;
	while (!(z == 27))	//czekanie na wciśniecie ENTER
	{
		z = _getch();
	}

	
}



void test()
{
	std::cout << "Please choose the generation method 1-EvenGenerator, 2-ExponentialGenerator, 3-NormalGenerator, 4-GeometricGenerator." << endl;
	int dis;
	cin >> dis;
	std::cout << "Please  write the number of iterations." << endl;
	int iter;
	cin >> iter;
	std::cout << "Please  write the number of kernel. Simulation for 55000" << endl;
	int kernel;
	cin >> kernel;

	Distribution* generate = new Distribution();
	fstream plik("distribution_data.txt", ios::out);
	if (plik.good())
	{
		int v = 0;
		double d = 0;
		for (int i = 0; i < iter; i++)
		{
			switch(dis)
			{
			case 1:
				v = static_cast<int>(generate->EvenGenerator(kernel, 0, 10));
				plik << v << '\n';
				break;
			case 2:
				d = generate->ExponentialGenerator(kernel, 1800);	
				plik << d << '\n';
				break;
			case 3:
				d = generate->NormalGenerator(kernel, 300, 1);
				d = round(d);
				plik << static_cast<int>(d) << '\n';
				break;
			case 4:
				d = generate->GeometricGenerator(kernel, 1/0.22);
				plik << d << '\n';
				break;
			default:
				std::cout << "Bot found generator" << endl;
				break;
				}
			
		}
	}
	plik.close();
	std::cout << "Data has been saved to file distribution_data.txt"<< endl;
	delete generate;
}

void write_state(fstream &state, int const time_system, int const number_of_recipients_served, int const number_of_blood_spent, int const act_length_queue_of_recipients, bool const act_emergency_order, int const j, int const act_clendar_size)
{
	state << time_system << "	" << number_of_recipients_served << "	" << number_of_blood_spent << "	" << act_length_queue_of_recipients << "	" << act_emergency_order <<"	"<<j<<"	" << act_clendar_size << '\n';
	
}

int kernels(Distribution* generate, int &kernel)
{
	double result = 0;
	for (int i = 0; i<100000; i++)
	{
		result = generate->EvenGenerator(kernel);
	}
	return static_cast<int>(result* 2147483647);
}

void generate_kernels( int const num, int const first_kernel)
{
	fstream kernels_file("kernels.txt", ios::out);
	int kernel = first_kernel;
	Distribution* generate = new Distribution(0);
	for (int i = 0; i<num; i++)
	{
		kernels_file << kernels(generate, kernel) << ' ' << kernels(generate, kernel) << ' ' << kernels(generate, kernel) << "	" << kernels(generate, kernel) << ' ' << kernels(generate, kernel) << ' ' << kernels(generate, kernel) << ' ' << kernels(generate, kernel) << '\n';
	}
	kernels_file.close();
}


//tryb debagowania (pojawianie się co się dzieje po kolei oraz tryb krokowy załączamy poprzez dyrektywę w DEBUG.h